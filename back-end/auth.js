const jwt = require('jsonwebtoken');
const User = require('./models/User');

module.exports.createAccessToken = (user) =>{
	let accessToken = jwt.sign({
			id: user._id,
			email: user.email
		}, process.env.SECRET);
	return accessToken;
}

module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;
	if(typeof token !== "undefined")
	{
		token = token.slice(7, token.length);
		jwt.verify(token, process.env.SECRET, (err, decoded)=>{
			if(!err) req.decodedToken = decoded
			return err ? res.send({auth: FAILED}):next()
		});
	}else{
		return res.send({auth: 'FAILED'});
	}
}