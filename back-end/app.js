const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const port = process.env.PORT || 4000;
const app = express();
require('dotenv').config();


mongoose.connect(process.env.DB_CONNECT, {useNewUrlParser: true, useUnifiedTopology: true});
mongoose.connection.on('error', () =>{console.log("Database is not connected.");});
mongoose.connection.once('open', ()=>{console.log("Database is connected");});

app.use(cors());
app.use(express.json());

const categoryRoutes = require('./routes/category');
app.use('/api/category', categoryRoutes);

const userRoutes = require('./routes/user.js');
app.use('/api/users', userRoutes);

const ledgerRoutes = require('./routes/ledger');
app.use('/api/ledger', ledgerRoutes);

app.listen(port, () =>{
	console.log(`System is online at port ${port}`);
})