import React, { useContext, useState } from 'react';
import { Container } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {Navbar, Nav} from 'react-bootstrap'
import Link  from 'next/link';
// import Router from 'next/router';
import UserContext from '../UserContext';

export default function SideNavBar() {

    const {user} =useContext(UserContext);
    // console.log(user)
    return (
        <Navbar bg="dark" variant="dark" expand="lg">
        <Link href='/'>
        <a className="navbar-brand">Planner</a>
        </Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
        {
            user.id === null
            ?
            <React.Fragment>
            <Link href="/">
            <a className="nav-link" role="Button"> Home </a>
            </Link>
            <Link href="/login">
            <a className="nav-link" role="Button"> Log-In </a>
            </Link>
            <Link href="/register">
            <a className="nav-link" role="Button"> Sign-Up/Register </a>
            </Link>
            <Link href="/about">
            <a className="nav-link" role="Button"> About </a>
            </Link>
            </React.Fragment>

            :
            <React.Fragment>
            <Link href={{pathname: '/user/[id]',query: { id: user.id},}} >
            <a className="nav-link" role="Button"> Home </a>
            </Link>
            <Link href="/categories">
            <a className="nav-link" role="Button"> Categories </a>
            </Link>
            <Link href="/user/transaction">
            <a className="nav-link" role="Button"> Transaction History </a>
            </Link>
            <Link href="/user/charts/monthly-expense">
            <a className="nav-link" role="Button"> Monthly Expense </a>
            </Link>
            <Link href="/user/charts/monthly-income">
            <a className="nav-link" role="Button"> Monthly Income </a>
            </Link>
            <Link href="/user/charts/balance-trend">
            <a className="nav-link" role="Button"> Trend </a>
            </Link>
            <Link href="/about">
            <a className="nav-link" role="Button"> About </a>
            </Link>
            {/*<Link href="">
            <a className="nav-link" role="Button"> Breakdown </a>
            </Link>*/}
            <Link href="/logout">
            <a className="nav-link" role="Button"> Sign Out </a>
            </Link>
            </React.Fragment>       
     }

     </Nav>
     </Navbar.Collapse>
     </Navbar>
     )
}